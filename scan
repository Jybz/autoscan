#!/bin/sh

# [jibz@jabztop ~]$ scanimage --help
# Usage: scanimage [OPTION]...
# 
# Start image acquisition on a scanner device and write image data to
# standard output.
# 
# Parameters are separated by a blank from single-character options (e.g.
# -d epson) and by a "=" from multi-character options (e.g. --device-name=epson).
# -d, --device-name=DEVICE   use a given scanner device (e.g. hp:/dev/scanner)
#     --format=pnm|tiff|png|jpeg  file format of output file
# -i, --icc-profile=PROFILE  include this ICC profile into TIFF file
# -L, --list-devices         show available scanner devices
# -f, --formatted-device-list=FORMAT similar to -L, but the FORMAT of the output
#                            can be specified: %d (device name), %v (vendor),
#                            %m (model), %t (type), %i (index number), and
#                            %n (newline)
# -b, --batch[=FORMAT]       working in batch mode, FORMAT is `out%d.pnm' `out%d.tif'
#                            `out%d.png' or `out%d.jpg' by default depending on --format
#     --batch-start=#        page number to start naming files with
#     --batch-count=#        how many pages to scan in batch mode
#     --batch-increment=#    increase page number in filename by #
#     --batch-double         increment page number by two, same as
#                            --batch-increment=2
#     --batch-print          print image filenames to stdout
#     --batch-prompt         ask for pressing a key before scanning a page
#     --accept-md5-only      only accept authorization requests using md5
# -p, --progress             print progress messages
# -n, --dont-scan            only set options, don't actually scan
# -T, --test                 test backend thoroughly
# -A, --all-options          list all available backend options
# -h, --help                 display this help message and exit
# -v, --verbose              give even more status messages
# -B, --buffer-size=#        change input buffer size (in kB, default 32)
# -V, --version              print version information
# 
# Options specific to device `dsseries:usb:0x04F9:0x60E0':
#   Scan mode:
#     --mode LineArt|Gray|Color [Gray]
#         Selects the scan mode (e.g., lineart, monochrome, or color).
#     --resolution 75..600dpi (in steps of 1) [300]
#         Sets the resolution of the scanned image.
#     --preview[=(yes|no)] [no]
#         Request a preview-quality scan.
#     --source ADF [ADF]
#         Selects the scan source (such as a document-feeder).
#   Geometry:
#     -l 0..215.9mm [0]
#         Top-left x position of scan area.
#     -t 0..355.6mm [0]
#         Top-left y position of scan area.
#     -x 0..215.9mm [215.9]
#         Width of scan-area.
#     -y 0..355.6mm [355.6]
#         Height of scan-area.
#   MultiFeed Detection:
#     --ultrasonic Disable [Disable]
#         Use ultrasonic sensor to detect multifeed
#   Enhancement:
#     --brightness -100..100% (in steps of 1) [0]
#         Controls the brightness of the acquired image.
#     --contrast -100..100% (in steps of 1) [0]
#         Controls the contrast of the acquired image.
#     --gamma-table 0..255,...
#         Gamma-correction table.  In color mode this option equally affects the
#         red, green, and blue channels simultaneously (i.e., it is an intensity
#         gamma table).
#     --red-gamma-table 0..255,...
#         Gamma-correction table for the red band.
#     --green-gamma-table 0..255,...
#         Gamma-correction table for the green band.
#     --blue-gamma-table 0..255,...
#         Gamma-correction table for the blue band.
# 
# Type ``scanimage --help -d DEVICE'' to get list of all options for DEVICE.


#Check software
hash scanimage
if [ 0 -ne ${?} ]; then
    echo "Scan image not found."
    exit
fi

#Check module SG is loaded
module=$(cat /proc/modules | grep sg)
if [ -z "${module}" ]; then
    echo "Start the module first as root : \`modprobe sg' "
fi

output="scan_${count}$(date +%y-%m-%d_%H-%M-%S).png"

# -b, --batch[=FORMAT]       working in batch mode, FORMAT is `out%d.pnm' `out%d.tif'
#                            `out%d.png' or `out%d.jpg' by default depending on --format
#     --batch-start=#        page number to start naming files with
#     --batch-count=#        how many pages to scan in batch mode
#     --batch-increment=#    increase page number in filename by #
#     --batch-double         increment page number by two, same as
#                            --batch-increment=2
#     --batch-print          print image filenames to stdout
#     --batch-prompt         ask for pressing a key before scanning a page

scanimage -d 'dsseries:usb:0x04F9:0x60E0' --mode color -p --format=png > ~/${output}
